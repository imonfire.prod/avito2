<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Ad;
use App\Category;

class AdsController extends Controller
{
    /**
     * Возвращает главную страницу со всеми объявлениями
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('main')->with('ads', Ad::all());
    }

    /**
     * Возвращает детальную страницу объявления
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($id) {
        return view('ad')->with('ad', Ad::findOrFail($id));
    }

    /**
     * Возвращает страницу создания объявления
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('create')->with('categories', Category::all());
    }

    /**
     * Сохраняет объявление и перенаправляет на главную страницу
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $ad = new Ad();
        $ad->name = $request->name;
        $ad->price = $request->price;
        $ad->description = $request->description;
        $ad->contact = $request->contact;
        $ad->address = $request->address;
        $ad->phone = $request->phone;
        $ad->email = $request->email;
        $ad->category_id = $request->category;
        $ad->user_id = Auth::id();
        $ad->save();

        return redirect('/');
    }

    /**
     * Удаляет объявление и перенаправляет на главную страницу
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request) {
        Ad::destroy($request->id);
        return redirect('/');
    }
}
