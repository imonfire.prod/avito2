@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <p>
                            {{ $ad->name }}
                            @if (auth()->id() == $ad->user->id)
                                <form id="deleteForm" action="{{ route('destroy') }}" class='float-right' method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $ad->id }}">
                                    <a style="cursor:pointer" class='float-right' onclick="document.getElementById('deleteForm').submit()">Удалить</a>
                                </form>
                            @endif
                        </p>
                        <p>Категория: {{ $ad->category->name }}</p>

                    </div>
                    <div class="card-body row">
                        <div class="col-md-8">
                            <img src="{{ $ad->img }}" alt="">
                            <p>Описание: {{ substr($ad->description, 0, 150) }}</p>
                        </div>
                        <div class="col-md-4">
                            <p><strong>Контакты</strong></p>
                            <p>Создатель: {{ $ad->contact }}</p>
                            <p>Email: {{ $ad->email }}</p>
                            <p>Телефон: {{ $ad->phone }}</p>
                            <hr>
                            <p>Цена: {{ $ad->price }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
