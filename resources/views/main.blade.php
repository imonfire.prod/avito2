@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($ads->count() <= 0)
            <div class="row justify-content-center mb-5 pt-5">
                <div class="col-md-10 text-center pt-5">
                    <p class="lead">Нет ни одного объявления (</p>
                </div>
            </div>
        @endif
        <?php $i = 0?>
        @foreach ($ads as $ad)
            <div class="row justify-content-center mb-5">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header">
                            {{ $ad->name }} <small>Категория: {{ $ad->category->name }}</small>
                            @if (auth()->id() == $ad->user->id)
                                <form id="deleteForm<?=$i?>" action="{{ route('destroy') }}" class='float-right' method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $ad->id }}">
                                    <a style="cursor:pointer" class='float-right' onclick="document.getElementById('deleteForm<?=$i?>').submit()">Удалить</a>
                                </form>
                            @endif
                        </div>
                        <div class="card-body">
                            <img src="{{ $ad->img }}" alt="">
                            <p>Описание: {{ substr($ad->description, 0, 150) }}</p>
                        </div>
                        <div class="card-footer">
                            <p style="margin-bottom: 0">Цена: {{ $ad->price }} <a href="/show/{{ $ad->id }}" class="float-right">Подробнее</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++?>
        @endforeach
    </div>
@endsection
